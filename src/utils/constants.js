// themesDefault 
export let Themes = {
    default: "default",
    dark: "dark",
};

// localStorage
export let LocalStorageKeys = {
    authToken: "auth_token",
    version: "version"
};
