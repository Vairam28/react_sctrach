import React from 'react';
import { TextField, Stack} from '@mui/material';
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';


export const TextBox = ({
    label_text = "",
    placeholder = "",
}) => {


    const Typography = styled('Typography')(({ theme }) => ({
        color: theme.palette.primary.main,
        fontSize: "16px",
        fontWeight:"bold"
    }));

    return <Box>
        <Stack direction="column" spacing={1}>
            <Typography variant="caption" >
                {label_text ?? ""}
            </Typography>
            <TextField size={'small'} id="outlined-basic" variant="outlined" placeholder={placeholder ?? ""} />
        </Stack>
    </Box>

}