import React from 'react';
import {Box,Button }from '@mui/material';

export const ButtonMode = ({
    text = "",
    variant = ""
}) => {


    return(
        <Box >
        <Button variant={variant?? ""} >{text}</Button>
</Box>   
    )   

}