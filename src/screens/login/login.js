import React from 'react';
import { Stack, Grid, Box } from '@mui/material';
import { LocalStorageKeys } from '../../utils';
import { useNavigate } from 'react-router-dom';
import { AppRoutes } from '../../router/routes';
import { TextBox, Password } from '../../components';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import { styled } from '@mui/material/styles';

export const Login = ({
    title = "Sign in",
    login = "Log In",
    alertButton = "Create Account",
    alertText = "Successfully Sigined In!"
}) => {

    const navigate = useNavigate();

    const onLogin = () => {
        localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
        navigate(AppRoutes.home);
    }

    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    const [open, setOpen] = React.useState(false);

    const handleClick = () => {

        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const Button = styled('Button')(({ theme }) => ({
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.primary.main,
        width:"100%",
        cursor: "pointer",
        padding: "8px",
        marginTop:"10px",
        borderRadius:"4px",
        border:"none",
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.primary.light,
            boxShadow: 'none',
          },
    })); 

    
    const Typography = styled('Typography')(({ theme }) => ({
        color: theme.palette.primary.main,
        fontSize: "24px",
        textAlign: "center",
        marginBottom: "16px",
        display: "flex",
        justifyContent: "center",
        fontWeight:"bold"
    }));


    return <div>

        <Grid container style={{ display: "flex", justifyContent: "center", height: "100vh", alignItems: "center"  }}>
            <Grid item xs={12} sm={12} md={4} lg={4} >
                <Box>
                    {/* title_text */}
                    <Typography variant="h6">
                        {title}
                    </Typography>
                    {/* title_text */}
                    {/* Inputbox_user */}
                    <Stack mb={1}>
                        <TextBox label_text="Mobile Number / Email ID" placeholder="abcd@gmail.com" />
                    </Stack>
                    {/* Inputbox_user */}

                    {/* password */}
                    <Stack mb={2}>
                        <Password label_text="Enter Password" placeholder="Enter ur password" />
                    </Stack>
                    {/* password */}

                    {/* siginin button */}
                    <Button variant="contained" fullWidth onClick={onLogin} type="submit" >
                        {login}
                    </Button>
                    {/* siginin button */}
                    
                    {/* create account button */}
                    <Button fullWidth variant="contained" onClick={handleClick} >{alertButton}</Button>
                    <Snackbar open={open} autoHideDuration={1500} onClose={handleClose} >
                        <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                            {alertText}
                        </Alert>
                    </Snackbar>
                    {/* create account button*/}
                </Box>
            </Grid>
        </Grid>
    </div>
}
