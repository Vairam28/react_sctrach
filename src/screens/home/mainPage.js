import React from 'react';
import { Typography } from '@mui/material';
import { LocalStorageKeys } from '../../utils';
import { useNavigate } from 'react-router-dom';
import { AppRoutes } from '../../router/routes';
import { styled } from '@mui/material/styles';

export const Welcomepage = ({val}) => {


    const navigate = useNavigate();

    const onLogin = () => {
        localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
        navigate(AppRoutes.login);
    }
    const Button = styled('Button')(({ theme }) => ({
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.primary.main,
        margin:"auto",
        display:"flex",
        cursor: "pointer",
        padding: "8px",
        marginTop:"10px",
        borderRadius:"4px",
        border:"none",
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.primary.light,
            boxShadow: 'none',
          },
    })); 

      const Box = styled('Box')(({ theme }) => ({
        ...theme.typography.button,
        textAlign:"center",
        marginTop:"80px",
        color: theme.palette.primary.light,
      }));  


    return <Box>
        <Typography variant="h5">
            Welcome
        </Typography>
        <Button onClick={onLogin}>
            Logout
        </Button>
    </Box>
}