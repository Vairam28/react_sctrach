import React from "react";
import { Welcomepage } from './mainPage';

class Home extends React.Component {
    render() {
        return <Welcomepage />;
    }
}

export default Home;
