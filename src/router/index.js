import React from "react";
import { BrowserRouter, Routes, Route, } from "react-router-dom";
import { AppRoutes } from "./routes";
import { Login } from './../screens';
import { Home } from './../screens'

const RouterApp = (props) => {
  
  return (
    <BrowserRouter>
      <Routes>
        {/* login Route */}
        <Route path={AppRoutes.login} element={<Login />} />
        {/* login Route */}
         {/* homepage Route */}
         <Route path={AppRoutes.home} element={<Home />} />
        {/* homepage Route */}
      </Routes>
    </BrowserRouter>
  );
};

export default RouterApp;
