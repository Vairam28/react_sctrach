import React from "react";
import { Themes } from "../utils";

// themeContext
export let ThemeContext = React.createContext({
  name: Themes.default,
  setTheme: () => null,
});
